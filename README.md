# h5预览文档

#### 介绍
前端实现在线预览pdf、word、xls、ppt等文件


参考网站
1.前端实现在线预览pdf、word、xls、ppt等文件
    https://www.jianshu.com/p/2f39de746900
2.用XDOC预览文档
    https://my.oschina.net/u/193624/blog/898989
3.联机查看 Office 文档
    注：支持的浏览器包括最新版本的 Microsoft Edge、Firefox、Chrome 和两个最新版本的 Safari。Office Online 支持的移动查看器包括 Windows Phone® 上的 Internet Explorer、Android™ 上的 Chrome，以及 iOS® 上两个最新版本的 Safari。请在此处 查看受支持浏览器的完整列表。
    https://products.office.com/zh-CN/office-online/view-office-documents-online?legRedir=true&CorrelationId=bf3aa625-1641-4640-a998-ccdda71b0490      

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)